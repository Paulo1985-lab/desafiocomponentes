package com.devsuperior.desafiocomponentes;

import java.util.Locale;
import java.util.Scanner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.devsuperior.desafiocomponentes.entities.Order;
import com.devsuperior.desafiocomponentes.services.OrderService;

@SpringBootApplication
public class DesafiocomponentesApplication implements CommandLineRunner {

	private final OrderService orderService;
	private final ConfigurableApplicationContext applicationContext;

	public DesafiocomponentesApplication(OrderService orderService, ConfigurableApplicationContext applicationContext) {
		this.orderService = orderService;
		this.applicationContext = applicationContext;
	}

	public static void main(String[] args) {
		SpringApplication.run(DesafiocomponentesApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		System.out.print("Código do pedido: ");
		int code = sc.nextInt();
		System.out.print("Valor básico: ");
		double basic = sc.nextDouble();
		System.out.print("Desconto: ");
		double discount = sc.nextDouble();

		Order order = new Order(code, basic, discount);
		double totalValue = orderService.calculateTotal(order);

		System.out.println("Pedido código: " + order.getCode());
		System.out.println("Valor total : " + totalValue);

		sc.close();
		applicationContext.close();
	}
}