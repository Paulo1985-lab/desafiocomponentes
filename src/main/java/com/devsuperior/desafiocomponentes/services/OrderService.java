package com.devsuperior.desafiocomponentes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devsuperior.desafiocomponentes.entities.Order;

@Service
public class OrderService {
	
	@Autowired
	private final ShippingService shippingService;
	
	@Autowired
	public OrderService(ShippingService shippingService) {
		this.shippingService = shippingService;
	}
	
	public double calculateTotal(Order order) {
		double discount = order.getBasic() * (order.getDiscount() / 100);
		double totalValue = order.getBasic() - discount;
		double totalValueOrder = totalValue + shippingService.Shipment(order);
		return totalValueOrder;
	}

}

