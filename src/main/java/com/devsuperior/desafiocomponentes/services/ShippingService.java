package com.devsuperior.desafiocomponentes.services;

import org.springframework.stereotype.Service;

import com.devsuperior.desafiocomponentes.entities.Order;

@Service
public class ShippingService {

	public double Shipment(Order order) {
		double basicVaule = order.getBasic();

		if (basicVaule < 100) {
			return 20.0;

		} else if (basicVaule >= 100 && basicVaule < 200) {
			return 12.0;
		} else {
			return 0.0;
		}

	}

}
